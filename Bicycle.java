//Gabriel Internoscia 1842167

public class Bicycle {
    //Field initialization
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    //Constructor
    public Bicycle(String manufacturerInput, int numberGearsInput, double maxSpeedInput){
        this.manufacturer = manufacturerInput;
        this.numberGears = numberGearsInput;
        this.maxSpeed = maxSpeedInput;
    }
    
    //Getter method for manufacturer
    public String getManufacturer(){
        return(this.manufacturer);
    }

    //Getter method for numberGears
    public int getNumberGears(){
        return(this.numberGears);
    }

    //Getter method for manufacturer
    public double getMaxSpeed(){
        return(this.maxSpeed);
    }

    //toString override
    public String toString(int i){
        return "Manufacturer: " + manufacturer + ", Number of gears: " + numberGears + ", MaxSpeed: " + maxSpeed;
    }
}