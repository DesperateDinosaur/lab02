//Gabriel Internoscia 1842167

public class BikeStore{
    public static void main(String[] args){
        //Creating an array of 4 bicycles
        Bicycle[] stock = new Bicycle [4];

        //Creating the 4 bicycles in the array
        stock[0] = new Bicycle("Manufacturer 1", 4, 20.0);
        stock[1] = new Bicycle("Manufacturer 2", 3, 15.0);
        stock[2] = new Bicycle("Manufacturer 3", 2, 25.0);
        stock[3] = new Bicycle("Manufacturer 4", 1, 20.0);

        //Printing out each object of the array
        for(int i = 0; i < 4; i++){
        System.out.println(stock[i].toString(i));
      }
    }
}